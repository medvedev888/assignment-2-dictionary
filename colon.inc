%define tmp 0
%macro colon 2
    %2: dq tmp
        db %1, 0
    %define tmp %2
%endmacro
