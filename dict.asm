global find_word

%define FOR_POINTER 8

section .text

find_word:
    push r14
    push r15
    mov  r14, rdi
    mov  r15, rsi
    .loop:
        test rsi, rsi
        jz .negative
        add rsi, FOR_POINTER
        mov rdi, r14
        call string_equals
        test rax, rax
        jz .positive
        mov rax, r15
        jmp .end
    .positive:
        mov  rsi, [r15]
        mov  r15, rsi
        jmp .loop
    .negative:
        xor  rax, rax
    .end:
        pop  r15
        pop  r14
        ret
