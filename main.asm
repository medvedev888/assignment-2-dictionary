%include "dict.inc"
%include "words.inc"
%include "lib.inc"

global _start

%define BUFFER_SIZE 255
%define FOR_POINTER 8
%define EXIT_CODE_ERROR 1

section .bss
buffer: resb 256

section .rodata
invalid_input: db "Key is invalid", 0
not_found: db "The key was not found in the dictionary", 0

section .text

_start:
    mov  rdi, buffer
    mov  rsi, BUFFER_SIZE
    call read_word
    test rax, rax
    jz  .invalid_input
    mov  rdi, buffer
    mov  rsi, tmp
    call find_word
    test rax,rax
    jz  .key_is_not_found
    lea  rdi, [rax + FOR_POINTER]
    call string_length
    add  rdi, rax
    inc  rdi
    call print_string
    xor rdi, rdi
    call  exit
    .invalid_input:
        mov  rdi, invalid_input
        call print_error
        mov rdi, EXIT_CODE_ERROR
        call  exit
    .key_is_not_found:
        mov  rdi, not_found
        call print_error
        mov rdi, EXIT_CODE_ERROR
        call  exit
