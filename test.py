import subprocess

input_strings = ["third_word", "jadhsghjfgadfhhajbdfjabnmfbewbhbfiabnasbfkdabjkfbjkanfenm diofcnvnmadishvadnskflnalkfnjka enf ma efnajeknfjkanefjadkjsfnjkabvbauoxhalvanklsdkfjkadkfdakslfnkjkdlsmflakfnksdnfskdlfnsdfksdfklsndfjknsdnfkjsnjdfnsnfksdnfksfnksdnlfnskdnfksndklfnsdkfnklsdflksdfndsfsdkladfnkandfmnam,ndfnadfnkadnfakdnfnadnsfkadkfnnakdfnkaldfnladfnklas", "first_word", ""]

output_strings = ["third word explanation", "",  "first word explanation", ""]

error_strings = ["", "The key was not found in the dictionary", "", "Key is invalid"]

for i in range(len(input_strings)):

    p = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    stdout, stderr = p.communicate(input = input_strings[i].encode())

    str_out = stdout.decode().strip()
    str_err = stderr.decode().strip()

    if str_out == output_strings[i] and str_err == error_strings[i]:

        print("Test " + str(i + 1) + " passed")

    else:

        print("Test " + str(i + 1) + ' failed. ./main print {strout: "' + str_out + '", strerr: "' + str_err + '"}, expected: {strout: "' + output_strings[i] + '", strerr: "' + error_strings[i] + '"}')
