NASM = nasm -f elf64 -o
LD = ld
RM = rm
PYTHON = python3

main: main.o lib.o dict.o
        $(LD) -o $@ $^

main.o: main.asm lib.o dict.o words.inc colon.inc
        $(NASM) $@ $<

dict.o: dict.asm lib.o
        $(NASM)  $@ $<

%.o: %.asm
        $(NASM)  $@ $<

test:
        $(PYTHON) test.py

clean:
        $(RM) -f *.o main
